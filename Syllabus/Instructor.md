## Instructor Information

- Stoney Jackson, PhD
- Email: [Stoney.Jackson@wne.edu](mailto:Stoney.Jackson@wne.edu)
- Appointments: [https://stoneyjackson.youcanbook.me/](https://stoneyjackson.youcanbook.me/)
- Office: H201b (during COVID all meetings will be on Discord)

### Office Hours

- M-F: 10:30-11:30a

All times are Eastern Time.

No appointment is necessary t9 reach me during these times.

Just pop into our class Discord server an get my attention by placing @Stoney
in any text channel. If the matter is private, direct message me on Discord.

### Outside Office Hours

You may also contact me outside office hours on Discord, by phone/text, or by email.
Please allow up to 1 business day for a response.

### Appointments

If you need to meet with me outside office hours, and it is too important to leave to chance
meetings, then make an appointment using the link below.

- https://stoneyjackson.youcanbook.me/

When you book an appointment, you will be sent a confirmation email. It may mention something
about Zoom. You can safely ignore that, we will meet in our class's Discord server.
