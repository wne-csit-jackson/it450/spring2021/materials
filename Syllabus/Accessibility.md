## Student Accessibility Services

Western New England University is committed to providing equitable access
to learning opportunities to students with documented disabilities.
It is the policy and practice of the University, consistent with federal
and state law, to ensure equal access to courses, course materials, and
course content for all students.  The University is committed to providing
reasonable and appropriate accommodations for all students with disabilities.

Individuals who have any situation/condition or disability diagnosis, either
permanent or temporary, which might affect their ability to perform in class
or access class materials, are encouraged to begin the accommodation request
process with Student Accessibility Services (SAS).  Accommodations are not
provided retroactively, and students are encouraged to register with SAS as
soon as possible to access all resources available for consistent support and
access to SAS programs.  Please contact the SAS office directly for more
information (see below).

```
Student Accessibility Services
Herman Hall, Suite 105
1215 Wilbraham Road
Springfield, MA  01119
Office: 413-782-1258
Fax:  413-782-1575
Email: accessibility@wne.edu
```
