## Academic Honesty

Participating in cheating will be reported to the chair who will determine an
appropriate penalty.

If you are not sure if a particular action would constitute cheating, it is
better to avoid the action until you can discuss it with your instructor.
