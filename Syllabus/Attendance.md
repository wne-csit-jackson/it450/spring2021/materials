## Attendance

* Attendance is important and required.
* All class sessions will be conducted using Discord.
* You are present and participating if you are interacting on Discord during class,
  and using a microphone and sharing your screen when asked to do so. A web-cam
  is not needed.
* Roll will regularly be taken, but will not directly count towards your grade.
* Should you need to or prefer to use our physical computer classroom, you may.
  If you do, be sure to bring and use a headset so that you can hear and be heard
  clearly without disrupting others in the same room.
* When working in teams, even if all of your team members are physically in the
  computer classroom, you should not sit together and are required to interact
  solely using Discord. This will help ensure that social distancing is maintained,
  that your team develops and improves skills for working online and remotely,
  in the event that any number of your team attends remotely your team will
  automatically be ready to accommodate them, and will allow your instructor to
  safely help your team during activities.
* If there are not enough seats while observing social distancing, if you able to
  attend remotely, you may be asked to do so.
