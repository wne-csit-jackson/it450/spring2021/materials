## Tentative Schedule

Avoid working ahead as topics, activities, assignments, and deadlines may change.

Week | Date | Day | Topic | Due
:--: | :--: | :--: | :--: | :--:
1 | 01-26 | T | Welcome back and setup |
1 | 01-28 | R | Implement REST API |
  |       |   | |
2 | 02-02 | T | Implement REST API |
2 | 02-04 | R | Implement REST API |
  |       |   | |
3 | 02-09 | T | Implement REST API |
3 | 02-11 | R | Implement REST API |
  |       |   | |
4 | 02-16 | T | Test REST API | Homework 1
4 | 02-18 | R | Test REST API | REVIEW
  |       |   | |
5 | 02-23 | T | EXAM 1 | EXAM 1
5 | 02-25 | R | Test REST API |
  |       |   | |
6 | 03-02 | T | Test REST API |
6 | 03-04 | R | Test REST API |
  |       |   | |
7 | 03-09 | T | Test REST API |
7 | 03-11 | R | Test REST API |
  |       |   |
8 | 03-16 | T | Vue | Homework 2
8 | 03-18 | R | Vue | REVIEW
  |       |   |
9 | 03-23 | T | EXAM 2 | EXAM 2
9 | 03-25 | R | Vue |
  |       |   |
10 | 03-30 | T | Vue |
10 | 04-01 | R | NO CLASS | NO CLASS
   |       |   |
11 | 04-06 | T | Vue |
11 | 04-08 | R | Vue |
   |       |   |
12 | 04-13 | T | Auth (or Front End Testing) | Homework 3
12 | 04-15 | R | Auth (or Front End Testing) | REVIEW
   |       |   |
13 | 04-20 | T | EXAM 3 | EXAM 3
13 | 04-22 | R | Auth (or Front End Testing) |
   |       |   |
14 | 04-27 | T | Auth (or Front End Testing) |
14 | 04-29 | R | Auth (or Front End Testing) | REVIEW
   |       |   |
15 | 05-04 | T | FINAL @ 1 | Homework 4

Final exam schedule for Spring 2021: <http://www.wne.edu/acadsched/finals/sp21finalnocombo.html>
