## IT-450 Advanced Topics of Web Design and Development

This course is a study of current advanced topics in Web design and development.
Topics such as load balancing, quality of service, caching, information architecture,
Web site administration tools, usability, and security in ecommerce will be studied.

3 cr.

Requisites: Complete IT-350 - Must be completed prior to taking this course.

### Objectives

* Able to implement a web application using a microservice architecture.
* Use Docker to build images.
* Use Docker Compose to configure a service composed of components.
* Use OpenAPI to design a REST API.
* Use ExpressJS to implement a REST API (specifically Express-OpenAPI-Validator).
* Use MongoDB to persist data.
* Use Mocha, Chai, and Axios to test a REST API implementation.
* Use Vue to implement a web-based front end.
* Use Git to manage source code.

If time allows...

* Use automated testing tools to test the front end.
* Add authentication and authorization.
