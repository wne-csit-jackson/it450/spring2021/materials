## Assessment

- 50% Exams (3-4)
- 50% Homework (3-4) and Reflection Quizzes (~1/wk)

### Grading Scale

The following scale is used to map overall scores to letter grades. The top is
the minimum overall percentage needed to earn the letter grade below. Overall
scores are rounded to the nearest percent before converting to a letter grade.

0 |	60 | 67 | 70 | 73 | 77 | 80 | 83 | 87 | 90 | 93
-- | -- | -- | -- | -- | -- | -- | -- | -- | -- | --
F |	D | D+ | C- | C | C+ | B- | B | B+ | A- | A


### Exams

Exams assess your knowledge of concepts, terminology, and their correct
application. Each exam primarily covers the material since the last exam.


### Homework and Reflection Quizzes

Individually, you will complete assignments. These will be used to deepen your
knowledge and assess your ability to apply your knowledge.

Along side these assignments you will be asked to keep up with readings and other
small tasks and complete reflection quizzes that give you the chance to ask questions
and/or reflect on the reading or task. Questions and comments from these reflection
quizzes will be used to drive discussions in class. There will be approximately
1-2 per week. These must be answered on time and will count toward the next assignment
submitted.

### Late Work

Unless otherwise specified, work is due by 9 AM ET, on the designated due date.

Quizzes and exams must be completed on time.

For homework, a 5% penalty per day late will be applied for the first 2 days late.
On the 3<sup>rd</sup> day late, late work will receive no more than 55%. It will be
checked for authenticity and completeness, and will not receive critical
feedback. Homework more than 1 week late will receive a 0.  Work not submitted
correctly is considered not submitted. Late work will not be accepted after the
final exam (or the last day of class if there is no final), and will receive a 0.

### Submission Guidelines

Different assignments may have different submission requirements. Be sure to pay
close attention to them. If you do not submit an assignment appropriately it may
not exist from the instructors point of view, and therefore it is not officially
submitted and may receive a 0 and be subject to late penalties.
